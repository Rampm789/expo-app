
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity,KeyboardAvoidingView, Alert ,BackHandler } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { widthToDp , heightToDp } from '../responsive/constant'


export default function Login(){


  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        BackHandler.exitApp()
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

    return(
        <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.logosec}>
        <Image style={styles.logo} source={{ uri: "https://logos-world.net/wp-content/uploads/2020/12/Lays-Logo.png" }} />
      </View>
      
      <View style={styles.inputs}>
      <KeyboardAvoidingView>
        <View style={styles.forminput}>
          <Text style={styles.inputlable}>Email Address</Text>
          <TextInput style={{ marginLeft: widthToDp(5.5),fontSize:widthToDp(4) }} editable value={"Username@gmail.com"} />
        </View>
        
        <View style={styles.forminput}>
          <Text style={styles.inputlable}>Password</Text>
          <TextInput style={{ marginLeft:  widthToDp(5.5),fontSize:widthToDp(4) }} editable value={"123456789"} secureTextEntry={true} />
        </View>
        <TouchableOpacity>
        <View style={{height: heightToDp(8),backgroundColor:"#3D4886",width:widthToDp(82),borderRadius:50,marginTop:heightToDp(3),elevation:1}} >
          <Text style={{color:"white",fontSize:widthToDp(5),marginLeft:widthToDp(35),marginTop:heightToDp(2)}}>Login</Text>
          </View>
        </TouchableOpacity>
        <View style={{flexDirection:"row",marginTop:heightToDp(3)}}>
        <TouchableOpacity>
        <Text style={{fontSize:widthToDp(4),paddingRight:widthToDp(38)}}>Signup</Text>
        </TouchableOpacity>
        <TouchableOpacity>
        <Text style={{fontSize:widthToDp(4)}}>Forgot Password?</Text>
        </TouchableOpacity>
        </View>
        </KeyboardAvoidingView>
      </View>
      
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F2F8FD'
    },
    logosec: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    logo:{
         height: heightToDp(15), width: widthToDp(29), marginTop: heightToDp(10) 
    },
    inputs: {
      flex: 2,
      alignItems: "center",
      marginTop: heightToDp(8)
    },
    forminput: {
      backgroundColor: "white", borderRadius: 20, height: heightToDp(11), width: widthToDp(82),marginTop:heightToDp(3),elevation:1
    },
    inputlable:{
        marginTop: heightToDp(2), marginLeft: widthToDp(5),fontSize:widthToDp(4),color:"grey"
    },
    input:{

    }
  });
