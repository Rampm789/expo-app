import React from 'react';
import { View, Text } from "react-native";
import * as SplashScreen from 'expo-splash-screen';

export default function splash({ navigation }){

  React.useEffect(() => {
    let timout=''
  async function prepare() {
      await SplashScreen.preventAutoHideAsync();
      setTimeout(()=>{
           SplashScreen.hideAsync().then(()=>{
            navigation.navigate('Login')
           })
      },3000)
  }
  prepare();
  return()=>{
      clearTimeout(timout)
  }
}, []);
  
    return(
        <View style={{flex:1,backgroundColor:"#3D4886",justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'white',fontSize:30}}>Lay's App 😀 </Text>
        </View>
    )
}