import { Dimensions, PixelRatio } from 'react-native'

const { width , height } = Dimensions.get('screen')

export const widthToDp= (val) =>{
    return PixelRatio.roundToNearestPixel(val * width / 100);
}

export const heightToDp= (val) =>{
    return PixelRatio.roundToNearestPixel(val * height / 100);
}



